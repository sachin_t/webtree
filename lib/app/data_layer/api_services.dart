import 'dart:developer';

import 'package:dio/dio.dart';
import 'package:get/get.dart';

import '../constants.dart';
import '../widgets/flush_bar.dart';

class ApiServices {

  ///API CALL
  static Future<dynamic> getWeatherData({String? cityName}) async {
    try {

      var params = {'q' : cityName, 'appid' : apiKey};

      var result = await dio.post(getWeatherUrl, queryParameters: params);

      log('response= ${result}');
      log('url= ${result.realUri}');

      if (result.statusCode == 200) {

        return result.data;

      }} on DioException catch(e) {

      if (e.response != null) {

        showFlushBar(context: Get.context, title: e.message.toString());

      } else {

        showFlushBar(context: Get.context, title: e.message.toString());

      }
    }
  }

}