import 'package:another_flushbar/flushbar.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

void showFlushBar({required String title, required BuildContext? context, bool isError = false}) {
  Flushbar(
      backgroundColor: isError? Colors.black87 :
      Colors.green,
      icon: isError ? const Icon(Icons.warning_amber_outlined,color: Colors.white):
      const SizedBox(),
      borderRadius: BorderRadius.all(Radius.circular(12.r)),
      margin: EdgeInsets.only(bottom: 20.h, left: 10.w, right: 10.h),
      isDismissible: true,
      duration: const Duration(seconds: 6),
      messageText: Text(title,
          style: const TextStyle(
          color: Colors.white,
          fontWeight: FontWeight.w600))).show(context!);

}
