import 'package:csc_picker/csc_picker.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import 'package:get/get.dart';

import '../../../widgets/flush_bar.dart';
import '../controllers/search_city_controller.dart';

class SearchCityView extends GetView<SearchCityController> {
  const SearchCityView({super.key});

  @override
  Widget build(BuildContext context) {
    return GetBuilder<SearchCityController>(
        builder: (c) =>
            Scaffold(
                body: Container(
                  height: double.infinity,
                  width: double.infinity,
                  decoration: const BoxDecoration(
                    gradient: LinearGradient(
                      begin: Alignment(0.22, -0.98),
                      end: Alignment(-0.22, 0.98),
                      colors: [Color(0xFF2E335A), Color(0xFF1C1B33)],
                    ),
                  ),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Text('Select your city',
                        style: TextStyle(
                            color: Colors.white,
                            fontWeight: FontWeight.w600,
                            fontSize: 22.sp
                        ),),
                      SizedBox(height: 10.h,),
                      Padding(
                        padding: EdgeInsets.symmetric(horizontal: 16.w),
                        child: CSCPicker(
                          onCountryChanged: (value) {
                            print(value);
                          },
                          onStateChanged: (value) {
                            print(value??'');
                          },
                          onCityChanged: (value) {
                            c.cityName.value = value??'';
                          },
                        ),
                      ),
                      SizedBox(height: 10.h,),
                      ElevatedButton(
                          style: const ButtonStyle(
                              backgroundColor: MaterialStatePropertyAll(Colors.blue)
                          ),
                          onPressed: () =>
                            c.cityName.value != ''?
                            c.getToWeatherPage():
                            showFlushBar(context: context, title: 'Please Select Your City'),
                          child: Text(
                            'Search',
                            style: TextStyle(
                                color: Colors.white,
                                fontSize: 17.sp,
                                fontWeight: FontWeight.w600
                            ),
                          ))
                    ],
                  ),
                ),
              )
    );
  }
}
