import 'package:get/get.dart';
import 'package:webtree/app/routes/app_pages.dart';

class SearchCityController extends GetxController {

  RxString cityName = ''.obs;

  ///NAVIGATING TO NEXT SCREEN
  getToWeatherPage() {

    Get.toNamed(Routes.WEATHER, arguments: cityName.value);

  }

}
