import 'package:get/get.dart';

import '../controllers/search_city_controller.dart';

class SearchCityBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<SearchCityController>(
      () => SearchCityController(),
    );
  }
}
