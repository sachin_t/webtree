import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import 'package:get/get.dart';

import '../controllers/weather_controller.dart';

class WeatherView extends GetView<WeatherController> {
  const WeatherView({super.key});

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
          body: Container(
            height: double.infinity,
            width: double.infinity,
            decoration: const BoxDecoration(
              gradient:LinearGradient(
                begin: Alignment(0.22, -0.98),
                end: Alignment(-0.22, 0.98),
                colors: [Color(0xFF2E335A), Color(0xFF1C1B33)],
              ),
            ),
            child: Stack(
              children: [
                SizedBox(
                    height: double.infinity,
                    width: double.infinity,
                    child: Image.asset('asset/images/background.png',fit: BoxFit.fill,)),
                GetX<WeatherController>(
                  builder: (c) {
                    return c.isLoading.value?
                        const Center(child: CircularProgressIndicator(color: Colors.white,)):
                    SingleChildScrollView(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            SizedBox(height: 90.h,),
                            Center(
                              child: Text(c.weatherData.value.name.toString(),
                                style: TextStyle(
                                    fontSize: 28.sp,
                                    color: Colors.white,
                                    fontWeight: FontWeight.w700
                                ),
                              ),
                            ),
                            SizedBox(height: 20.h,),
                            Center(
                              child: Text('${c.temperature??''}°C',
                                style: TextStyle(
                                    fontSize: 28.sp,
                                    color: Colors.white54,
                                    fontWeight: FontWeight.w700
                                ),
                              ),
                            ),
                            SizedBox(height: 30.h,),
                            Center(
                              child: Padding(
                                padding: EdgeInsets.symmetric(horizontal: 20.w),
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    Text('${c.weatherData.value.weather?[0].main}',
                                      style: TextStyle(
                                          fontSize: 20.sp,
                                          color: Colors.white70,
                                          fontWeight: FontWeight.w700
                                      ),
                                    ),
                                    SizedBox(width: 30.w,),
                                    c.weatherData.value.weather?[0].main == 'Clouds'?
                                    const Icon(Icons.cloud, color: Colors.white,):
                                    c.weatherData.value.weather?[0].main == 'Rain'?
                                    const Icon(Icons.shower, color: Colors.white,):
                                    c.weatherData.value.weather?[0].main == 'Haze'?
                                    const Icon(Icons.snowing, color: Colors.white,):
                                    c.weatherData.value.weather?[0].main == 'Sunny'?
                                    const Icon(Icons.sunny, color: Colors.white,):
                                    const Icon(Icons.cloud, color: Colors.white,)
                                  ],
                                ),
                              ),
                            ),
                            SizedBox(height: 30.h,),
                            Center(
                              child: Padding(
                                padding: EdgeInsets.symmetric(horizontal: 20.w),
                                child: c.weatherData.value.main != null?
                                Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    Text('Min Temperature: ${c.minTemp}°C',
                                      style: TextStyle(
                                          fontSize: 20.sp,
                                          color: Colors.white70,
                                          fontWeight: FontWeight.w700
                                      ),
                                    ),
                                    SizedBox(height: 20.h,),
                                    Text('Max Temperature: ${c.maxTemp}°C',
                                      style: TextStyle(
                                          fontSize: 20.sp,
                                          color: Colors.white70,
                                          fontWeight: FontWeight.w700
                                      ),
                                    ),
                                  ],
                                ) : const SizedBox(),
                              ),
                            ),
                            SizedBox(height: 30.h,),
                            Center(
                              child: Padding(
                                padding: EdgeInsets.symmetric(horizontal: 20.w),
                                child: c.weatherData.value.main != null?
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    Text('Humidity:  ',
                                      style: TextStyle(
                                          fontSize: 20.sp,
                                          color: Colors.white70,
                                          fontWeight: FontWeight.w700
                                      ),
                                    ),
                                    Text(c.weatherData.value.main!.humidity.toString(),
                                      style: TextStyle(
                                          fontSize: 20.sp,
                                          color: Colors.white70,
                                          fontWeight: FontWeight.w700
                                      ),
                                    ),
                                  ],
                                ) : const SizedBox(),
                              ),
                            ),
                            SizedBox(height: 30.h,),
                            Center(
                              child: Padding(
                                padding: EdgeInsets.symmetric(horizontal: 20.w),
                                child: c.weatherData.value.wind != null?
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    Text('Wind Speed:  ',
                                      style: TextStyle(
                                          fontSize: 20.sp,
                                          color: Colors.white70,
                                          fontWeight: FontWeight.w700
                                      ),
                                    ),
                                    Text(c.weatherData.value.wind!.speed.toString(),
                                      style: TextStyle(
                                          fontSize: 20.sp,
                                          color: Colors.white70,
                                          fontWeight: FontWeight.w700
                                      ),
                                    ),
                                  ],
                                ) : const SizedBox(),
                              ),
                            ),
                            SizedBox(height: 20.h,),
                          ]
                      ),
                    );
                  },
                )
              ],
            ),
          )
      ),
    );
  }
}
