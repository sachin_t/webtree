
import 'dart:convert';

import 'package:get/get.dart';
import 'package:webtree/app/data_layer/api_services.dart';

import '../../../data_layer/get_weather_data.dart';

class WeatherController extends GetxController {

  Rx<GetWeatherData> weatherData = GetWeatherData().obs;

  String? cityName = '';
  String? temperature = '';
  String? minTemp = '';
  String? maxTemp = '';

  RxBool isLoading = false.obs;

  @override
  void onInit() {
    super.onInit();

    initCall();

  }

  initCall() {

    if (Get.arguments != null) {

      cityName = Get.arguments;

      getWeatherData(cityName);

    }
  }

  ///API CALL FROM CONTROLLER
  void getWeatherData(cityName) async {

    isLoading.value = true;

    dynamic result = await ApiServices.getWeatherData(cityName: cityName);

    if (result != null) {

      weatherData.value = getWeatherDataFromJson(jsonEncode(result));

      if (weatherData.value.main != null) {
        temperature = (double.parse(weatherData.value.main!.temp.toString()) - 273.15).toString().substring(0, 5);
        minTemp = (double.parse(weatherData.value.main!.tempMin.toString()) - 273.15).toString().substring(0, 5);
        maxTemp = (double.parse(weatherData.value.main!.tempMax.toString()) - 273.15).toString().substring(0, 5);
      }

    }

    isLoading.value = false;

  }

}
