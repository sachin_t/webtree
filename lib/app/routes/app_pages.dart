import 'package:get/get.dart';

import '../modules/search_city/bindings/search_city_binding.dart';
import '../modules/search_city/views/search_city_view.dart';
import '../modules/weather/bindings/weather_binding.dart';
import '../modules/weather/views/weather_view.dart';

part 'app_routes.dart';

class AppPages {
  AppPages._();

  static const INITIAL = Routes.SEARCHCITY;

  static final routes = [
    GetPage(
      name: _Paths.SEARCHCITY,
      page: () => const SearchCityView(),
      binding: SearchCityBinding(),
    ),
    GetPage(
      name: _Paths.WEATHER,
      page: () => const WeatherView(),
      binding: WeatherBinding(),
    ),
  ];
}
