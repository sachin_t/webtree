part of 'app_pages.dart';

abstract class Routes {
  Routes._();
  static const SEARCHCITY = _Paths.SEARCHCITY;
  static const WEATHER = _Paths.WEATHER;
}

abstract class _Paths {
  _Paths._();
  static const SEARCHCITY = '/searchCity';
  static const WEATHER = '/weather';
}
